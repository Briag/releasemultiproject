package fr.formation.ejb;

import javax.ejb.Local;

@Local
public interface IStateful {
	
	public int compteur();

}
